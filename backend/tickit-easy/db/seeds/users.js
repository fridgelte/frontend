exports.seed = function(knex, Promise) {
  return knex('user_permissions').del()
    .then(function () {
      return knex('users').del()
    })
    .then(function () {
      const users = []
      const userNames = [
        {
          firstName: "Ivan",
          lastName: "Benedek",
          email: "bivan@inf.elte.hu",
          password: "$2y$12$3sJGumIdVX/D2E3qdpT6d.oEI6Vb.YoJ8q1ywKxy8gpPhRNINMfVS",
          status: 1 
        },
        {
          firstName: "Magyar",
          lastName: "Adam",
          email: "madam@inf.elte.hu",
          password: "$2y$12$3sJGumIdVX/D2E3qdpT6d.oEI6Vb.YoJ8q1ywKxy8gpPhRNINMfVS",
          status: 1 
        },
        {
          firstName: "Solti",
          lastName: "Peter",
          email: "fuogy5@inf.elte.hu",
          password: "$2y$12$3sJGumIdVX/D2E3qdpT6d.oEI6Vb.YoJ8q1ywKxy8gpPhRNINMfVS",
          status: 1 
        },
      ]

      for (let i = 0; i < 3; ++i) {
        users.push({
          firstName: userNames[i].firstName,
          lastName: userNames[i].lastName,
          email: userNames[i].email,
          password: userNames[i].password,
          status: userNames[i].status
        })
      }

      return knex('users').insert(users);
    })
};