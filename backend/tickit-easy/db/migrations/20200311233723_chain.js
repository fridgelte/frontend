
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('users', function (table) {
            table.increments('id').primary();
            table.string('username', 40);
            table.string('password', 120);
            table.string('email', 60);
            table.string('full_name', 60);
            table.boolean('status');
            table.boolean('is_admin');
            
            table.unique('id');
        })
        .createTable('view', function (table) {
            table.increments('id').primary();
            table.text('nev');
            table.string('orszag', 17);
            table.string('megye', 28);
            table.text('lakohely');
            table.text('email');
            table.text('tel');
            table.string('tabor', 45);
            table.enu('allapot', ['AKTIV', 'TOROLT', 'INAKTIV'])
            table.integer('jelentkezoid', 10)

            table.unique('id');
        })
        .createTable('destinations', function(table){
            table.increments('id').primary();
            table.string('name', 120);
            
            table.unique('id');
        })
        .createTable('ticket_category', function (table) {
            table.increments('id').primary();
            table.integer('destination', 11).unsigned();
            table.integer('default_price', 11);

            table.unique('id');
            table.foreign('destination').references('id').inTable('destinations');
        })
        .createTable('waiting_list', function(table){
            table.integer('camper_id').unsigned();
            table.integer('user_id').unsigned();
            table.integer('ticket_category_id').unsigned();
            table.text('comment');
            table.timestamps('created_at');

            table.foreign('ticket_category_id').references('id').inTable('ticket_category');
            table.foreign('user_id').references('id').inTable('users');
            table.foreign('camper_id').references('id').inTable('view');
        })
        .createTable('transactions', function(table){
            table.increments('id').primary();
            table.integer('camper_id', 11);
            table.integer('user_id', 11);
            table.integer('ticket_id', 11);
            table.boolean('is_returned');
            table.timestamps('created_at');
            
            table.unique('id');
        })
        .createTable('log', function(table){
            table.increments('id').primary();
            table.timestamps('created_at');
            table.text('description');

            table.unique('id');
        });
}

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('log')
        .dropTable('transactions')
        .dropTable('waiting_list')
        .dropTable('ticket_category')
        .dropTable('destinations')
        .dropTable('view')
        .dropTable('users');
}